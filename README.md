# Retry

This project is a sample to demonstrate the different usages of Retry API from Spring.


## Start it

run the project with docker: 

`docker compose up` (I advice to not detach (-d option) so you can see the logs)

or launch it from maven:

`mvn spring-boot:run`

or from your favorite IDE


swagger location to check every Endpoint used to demonstrate the projet (but keep in mind that most of what is going on will be shown in the logs):  
http://localhost:8080/swagger-ui.html

## Documentation of Spring Retry

you will find multiple tutorials or documentation about this lib, but the best documentation I have found so far is the github repository of Spring-retry:  
https://github.com/spring-projects/spring-retry

## What is shown

### how to use the simple Retry annotation

logic in class `com.fdg.retriablewithspring.service.simpleannotation.SimpleRetryableAnnotationService.java`

### how to use the programmatic way of retry mechanism

logic in class `com.fdg.retriablewithspring.service.programmaticway.ProgrammaticRetryableService.java`

### how to create a custom Retry annotation

logic in package `com.fdg.retriablewithspring.service.customannotation` 

### how to combine retry programmatic way with custom aspect to gain better control

This is the best sweat spot between powerfull and convenient way to use Retryable. It's basically about creating a custom aspect using the RetryTemplate to replace the @Retryable annotation

logic in package `com.fdg.retriablewithspring.service.customaspect`

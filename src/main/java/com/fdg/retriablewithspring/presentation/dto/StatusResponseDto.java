package com.fdg.retriablewithspring.presentation.dto;

public record StatusResponseDto(String result, Integer attempts) {
}

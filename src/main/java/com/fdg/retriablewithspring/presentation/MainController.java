package com.fdg.retriablewithspring.presentation;

import com.fdg.retriablewithspring.presentation.dto.StatusResponseDto;
import com.fdg.retriablewithspring.service.customannotation.CustomRetryableAnnotationService;
import com.fdg.retriablewithspring.service.customaspect.CustomAspectRetryableService;
import com.fdg.retriablewithspring.service.programmaticway.ProgrammaticRetryableService;
import com.fdg.retriablewithspring.service.simpleannotation.SimpleRetryableAnnotationService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(("/api"))
public class MainController {

    private final SimpleRetryableAnnotationService simpleRetryableAnnotationService;
    private final CustomRetryableAnnotationService customRetryableAnnotationService;
    private final ProgrammaticRetryableService programmaticRetryableService;
    private final CustomAspectRetryableService customAspectRetryableService;

    public MainController(SimpleRetryableAnnotationService simpleRetryableAnnotationService, CustomRetryableAnnotationService customRetryableAnnotationService, ProgrammaticRetryableService programmaticRetryableService, CustomAspectRetryableService customAspectRetryableService) {
        this.simpleRetryableAnnotationService = simpleRetryableAnnotationService;
        this.customRetryableAnnotationService = customRetryableAnnotationService;
        this.programmaticRetryableService = programmaticRetryableService;
        this.customAspectRetryableService = customAspectRetryableService;
    }

    @GetMapping("/simple-annotation")
    public StatusResponseDto retryWithSimpleAnnotation() {
        return simpleRetryableAnnotationService.fetchWithSimpleRetryAnnotation();
    }

    @GetMapping("/custom-annotation")
    public StatusResponseDto retryWithCustomAnnotation() {
        return customRetryableAnnotationService.fetchWithCustomRetryableAnnotation();
    }

    @GetMapping("/programmatic-way")
    public StatusResponseDto retryWithProgrammaticWay() {
        return programmaticRetryableService.fetchWithSimpleRetryAnnotation();
    }

    @GetMapping("/custom-way")
    public StatusResponseDto retryWithCustomAspectWay() {
        return customAspectRetryableService.fetchWithCustomAspect();
    }
}

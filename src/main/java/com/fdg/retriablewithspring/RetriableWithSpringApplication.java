package com.fdg.retriablewithspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RetriableWithSpringApplication {

    public static void main(String[] args) {
        SpringApplication.run(RetriableWithSpringApplication.class, args);
    }

}

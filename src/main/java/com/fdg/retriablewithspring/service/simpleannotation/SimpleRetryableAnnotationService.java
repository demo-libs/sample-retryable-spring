package com.fdg.retriablewithspring.service.simpleannotation;

import com.fdg.retriablewithspring.presentation.dto.StatusResponseDto;
import com.fdg.retriablewithspring.service.FetchException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;

@Service
public class SimpleRetryableAnnotationService {
    private static final Logger LOGGER = LoggerFactory.getLogger(SimpleRetryableAnnotationService.class);

    private static final int MAX_ATTEMPTS = 2;

    @Recover
    public StatusResponseDto recoverFetchWithSimpleRetryAnnotation(FetchException e) {
        return new StatusResponseDto("failed after too many attemps, reason: " + e.getMessage(), MAX_ATTEMPTS);
    }

    @Retryable(retryFor = FetchException.class, maxAttempts = MAX_ATTEMPTS, recover = "recoverFetchWithSimpleRetryAnnotation")
    public StatusResponseDto fetchWithSimpleRetryAnnotation() {
        //wait a bit to simulate a remote fetch
        try {
            Thread.sleep(100L);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        if (Math.random() < 0.2) {
            //with a simple @Retryable annotation, there is no simple way to store any context about retry operations. listeners could be a way to add some logic but that's it.
            return new StatusResponseDto("success", null);
        } else {
            LOGGER.warn("we failed to fetch datas");
            throw new FetchException("you failed to fetch... try again");
        }
    }
}

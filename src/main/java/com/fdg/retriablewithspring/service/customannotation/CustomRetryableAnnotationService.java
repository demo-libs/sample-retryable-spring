package com.fdg.retriablewithspring.service.customannotation;

import com.fdg.retriablewithspring.presentation.dto.StatusResponseDto;
import com.fdg.retriablewithspring.service.FetchException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class CustomRetryableAnnotationService {
    private static final Logger LOGGER = LoggerFactory.getLogger(CustomRetryableAnnotationService.class);

    /**
     * this fetch is retried thanks to a custom annotation wrapping some logic to avoid repeating it everywhere we need it. but there is the same limitations as @Retryable.
     * @return
     */
    @CustomRetryable
    public StatusResponseDto fetchWithCustomRetryableAnnotation() {
        //wait a bit to simulate a remote fetch
        try {
            Thread.sleep(100L);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        if (Math.random() < 0.2) {
            //with a simple @Retryable annotation, there is no simple way to store any context about retry operations. listeners could be a way to add some logic but that's it.
            return new StatusResponseDto("success", null);
        } else {
            LOGGER.warn("we failed to fetch datas");
            throw new FetchException("you failed to fetch... try again");
        }
    }
}

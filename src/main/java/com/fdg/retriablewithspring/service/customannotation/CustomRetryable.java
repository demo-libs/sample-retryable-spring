package com.fdg.retriablewithspring.service.customannotation;

import com.fdg.retriablewithspring.service.FetchException;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;

import java.lang.annotation.*;

@Retryable(maxAttempts = 10, backoff = @Backoff(delay = 150L), retryFor = FetchException.class)
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface CustomRetryable {
}

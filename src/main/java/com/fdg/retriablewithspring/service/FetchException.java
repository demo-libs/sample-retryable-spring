package com.fdg.retriablewithspring.service;

public class FetchException extends RuntimeException{
    public FetchException(String message) {
        super(message);
    }

    public FetchException(Throwable cause) {
        super(cause);
    }
}

package com.fdg.retriablewithspring.service.programmaticway;

import com.fdg.retriablewithspring.presentation.dto.StatusResponseDto;
import com.fdg.retriablewithspring.service.FetchException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.retry.RetryContext;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.stereotype.Service;

@Service
public class ProgrammaticRetryableService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProgrammaticRetryableService.class);
    /**
     * the retry template allows us to pass a context to store some information between each try
     */
    private final RetryTemplate retryTemplate = RetryTemplate.builder()
            .maxAttempts(3)
            .fixedBackoff(300)
            .retryOn(FetchException.class)
            .build();

    public StatusResponseDto fetchWithSimpleRetryAnnotation() {

        return retryTemplate.execute(
                context -> doFetch(context),
                context -> {
                    LOGGER.info("we failed too many times.");
                    // here you can add some business logic to handle multiple fails
                    // and also retrieve the data stored in contest:
                    LOGGER.info("we stored this: " + context.getAttribute("custom-data"));
                    return new StatusResponseDto("failed after too many attemps, reason: " + context.getLastThrowable().getMessage(), context.getRetryCount());
                });


    }

    private StatusResponseDto doFetch(RetryContext context) {
        //wait a bit to simulate a remote fetch
        try {
            Thread.sleep(100L);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        if (Math.random() < 0.2) {
            //with a simple @Retryable annotation, there is no simple way to store any context about retry operations. listeners could be a way to add some logic but that's it.
            return new StatusResponseDto("success", context.getRetryCount());
        } else {
            //here we can store some data in context!
            context.setAttribute("custom-data", 42);
            LOGGER.warn("we failed to fetch datas");
            throw new FetchException("you failed to fetch... try again");
        }
    }
}

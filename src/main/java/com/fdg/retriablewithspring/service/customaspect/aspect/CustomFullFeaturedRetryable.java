package com.fdg.retriablewithspring.service.customaspect.aspect;

import java.lang.annotation.*;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface CustomFullFeaturedRetryable {
}

package com.fdg.retriablewithspring.service.customaspect;

import com.fdg.retriablewithspring.presentation.dto.StatusResponseDto;
import com.fdg.retriablewithspring.service.customaspect.aspect.CustomFullFeaturedRetryable;
import com.fdg.retriablewithspring.service.FetchException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class CustomAspectRetryableService {
    private static final Logger LOGGER = LoggerFactory.getLogger(CustomAspectRetryableService.class);

    @CustomFullFeaturedRetryable
    public StatusResponseDto fetchWithCustomAspect() {
        //wait a bit to simulate a remote fetch
        try {
            Thread.sleep(100L);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        if (Math.random() < 0.2) {
            //as with a simple @Retryable annotation, there is no simple way to store any context about retry operations.
            // but we can change datas returned thanks to this aspect!
            return new StatusResponseDto("success", null);
        } else {
            //here we can NOT store some data in context!
            //but we can do it in the aspect
            LOGGER.warn("we failed to fetch datas");
            throw new FetchException("you failed to fetch... try again");
        }
    }
}

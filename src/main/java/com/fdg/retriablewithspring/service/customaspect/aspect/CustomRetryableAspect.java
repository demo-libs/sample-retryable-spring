package com.fdg.retriablewithspring.service.customaspect.aspect;

import com.fdg.retriablewithspring.presentation.dto.StatusResponseDto;
import com.fdg.retriablewithspring.service.FetchException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class CustomRetryableAspect {
    private static final Logger LOGGER = LoggerFactory.getLogger(CustomRetryableAspect.class);
    private final RetryTemplate retryTemplate = RetryTemplate.builder()
            .maxAttempts(3)
            .fixedBackoff(300)
            .retryOn(FetchException.class)
            .build();

    @Around("@annotation(CustomFullFeaturedRetryable)")
    public Object retriableAspect(final ProceedingJoinPoint proceedingJoinPoint) throws Throwable {

        return retryTemplate.execute(
                ctx -> {
                    ctx.setAttribute("custom-data", 42);
                    StatusResponseDto result = (StatusResponseDto) proceedingJoinPoint.proceed();
                    return new StatusResponseDto(result.result(), ctx.getRetryCount());

                },
                context -> {
                    LOGGER.info("we failed too many times.");
                    // here you can add some business logic to handle multiple fails
                    // and also retrieve the data stored in contest:
                    LOGGER.info("we stored this: " + context.getAttribute("custom-data"));
                    //Be careful, as we cannot conveniently know what return type the proceedingJoinPoint is supposed to return. Here all method annotated with @CustomFullFeaturedRetryable should return a subtype of StatusResponseDto.
                    return new StatusResponseDto("failed after too many attemps, reason: " + context.getLastThrowable().getMessage(), context.getRetryCount());
                });
    }
}

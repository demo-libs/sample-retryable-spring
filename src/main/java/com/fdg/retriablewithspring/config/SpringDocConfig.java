package com.fdg.retriablewithspring.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SpringDocConfig {

    @Bean
    OpenAPI api() {
        return new OpenAPI().info(new Info()
                .contact(new Contact()
                        .name("François de Guibert")
                        .email("fdeguibert@takima.fr"))
                .title("Retryable-sample")
                .description("demo the usage of retryable mechanism from spring")
                .version("v1.0.0"));
    }
}
